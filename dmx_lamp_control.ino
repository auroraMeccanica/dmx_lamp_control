/*
Copyright © 2023 auroraMeccanica, Massimo Gismondi

This file is part of dmx_lamp_control.
 
dmx_lamp_control is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Vengono utilizzate due librerie esterne:
- il generatore di segnale è come pubblico dominio,
  realizzato da Stefan Gustavson
  https://github.com/stegu
- libreria per invio comandi DMX su esp8266,
  realizzata da Matthew Tong e pubblicata
  con licenza libera GNU GPL versione 3
  o successiva
  https://github.com/mtongnz/espDMX
*/

// Esce sul D4 il DMX
// Entra sul D6 il pulsante

volatile static bool should_start_play = false;
volatile static bool is_playing = false;
#define PIN_BUTTON D6

#include "voce.h"
//#include "armnoise8.h"
#include <espDMX.h>

#define CAMPIONI_RMS_AL_SECONDO 10
const float DURATA_CAMPIONE_RMS = 1000.0 / CAMPIONI_RMS_AL_SECONDO;

// Mantiene in ram i valori dei canali da inviare
// all'istante attuale
static byte valori_riga_ram[NUM_DMX_CHANNELS] = {0};

// Per evitare di accedere alla flash a ogni iterazione
// guardo se la lettura è necessaria
int last_access_index_computed = -1;

unsigned long start_millis = 0;


// Le ISR vanno mantenute in RAM
ICACHE_RAM_ATTR void isr_start_play()
{
  should_start_play=true;
}

void setup()
{  
  pinMode(PIN_BUTTON, INPUT_PULLUP);
  attachInterrupt(
    digitalPinToInterrupt(PIN_BUTTON),
    isr_start_play,
    FALLING);

  
  // Esce sul D4
  dmxB.begin();

  dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);
}

void loop()
{
  // Se non sta suonando
  if (!is_playing)
  { 
    if (should_start_play)
    {
      // Ma deve iniziare a farlo
      start_millis = millis();
      should_start_play = false;
      is_playing = true;
      Serial.println("Start");
    }
    else
    {
      // Né deve iniziare...
      return;
    }
  }
  int chosen_index;
  {
    unsigned long time_passed = millis()-start_millis;

    float index_no_offset = time_passed / DURATA_CAMPIONE_RMS;
    
    // Tolgo 0.5 per ricentrare la media sul campione
    int tmp_index = floor(index_no_offset - 0.5);

    chosen_index = constrain(
      tmp_index,
      0,
      NUM_OF_SAMPLES // PERMETTO VOLONTARIAMENTE
      // Che vada fuori dall'array.
      // Se è così, smetto di inviare segnali dmx alla lampada
    );
  }
  if (chosen_index == NUM_OF_SAMPLES)
  {
    // Non serve né trasferire memoria, né
    // inviare altri segnali dmx
    is_playing = false;
    return;
  }


  // Se non sto eseguendo nessuna sequenza
  // poiché è terminata,
  // oppure se non siamo ancora arrivati al
  // istante successivo,
  // evito di andare a leggere la memoria flash
  // ed esco prematuramente
  if (last_access_index_computed != chosen_index)
  {
    // Copia i valori dalla memoria flash
    memcpy_P(valori_riga_ram, DATI_DMX[chosen_index], NUM_DMX_CHANNELS);

    last_access_index_computed = chosen_index;
  } 
  
  dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);
  delay(DURATA_CAMPIONE_RMS*0.75);
}
